require 'spec_helper'

describe Loan do
  it "returns a last loan" do
    user = User.create(name: "ololol",
                surname: "awdawdaw",
                secondname: "wadawdaw")
    l = user.loans.build(id:1, loan_sum: 10000)

    expect(user.loans.last).to eq(l)
  end

  it "returns a unpayed loans" do
    user = User.create(name: "ololol",
                       surname: "awdawdaw",
                       secondname: "wadawdaw")
    l = user.loans.create(id:1, loan_sum: 10000,
                         begin_date: Time.zone.today,
                         end_date: Time.zone.today + 6.months,
                         status: false)
    expect(user.loans.unpayed_loans).to eq(l)
  end
end